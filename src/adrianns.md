# Adriann's Basic Programming Challenges

- [Elementary](adrianns/elementary.md)
    - [Lists and Strings](adrianns/liststr.md)
- [Intermediate](adrianns/intermediate.md)
- [Advanced](adrianns/advanced)



