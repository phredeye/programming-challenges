# Challenges
- [Fred's Challenges](freds.md)
- [jRat's Challenges](jrats.md)
- [Adriann's Basic Challenges](adrianns.md)
    - [Elementary](adrianns/elementary.md)
        - [Lists and Strings](adrianns/liststr.md)
    - [Intermediate](adrianns/intermediate.md)
    - [Advanced](adrianns/advanced.md)
