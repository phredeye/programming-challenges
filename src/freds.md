# Fred's Challenges
# Question List

## Math / Numbers

1.  Loop / Sum (ignore, we did this yesterday)

  Set up an array to hold the following string values:

  - Beyonce (f)
  - David Bowie (m)
  - Elvis Costello (m)
  - Madonna (f)
  - Elton John (m)
  - Charles Aznavour (m)

  Write a program to loop round and count how many are male vocalists and how many are female. 

2. Write a function `apply_to_each()` that applies a function to every element of a list. Use it to print the first twenty perfect squares. The perfect squares can be found by multiplying each natural number with itself. The first few perfect squares are `1*1= 1`, `2*2=4`, `3*3=9`, `4*4=16`. 

Twelve for example is not a perfect square because there is no natural number m so that `m*m=12`. 

3. Write a guessing game where the user has to guess a secret number from 1-10. 

After every guess the program tells the user whether their number was too large or too small. 

At the end the number of tries needed should be printed. 

4.  Write a guessing game where the a program simulates 2 people in a guessing game:  

p1 randomly chooses a secret number between 1 and 10, then the p2 program has to guess that number. 

p2's first guess should be random, the p1 program should answer if the number is too high or too low.  

p2 should be able to guess the answer within 4 guesses by keeping track of previous guesses and by dividing the remaining number range by 2.

e.g. 

> p1 sets the secret number as 7.
> p2 guesses 3. 
> p1 says: too low.
> p2 knows the range is `> 3 && <= 10` so the next guess is a range of `7 / 2 + 3 = (3.5 + 3) = floor(6.5) = 6`
> p1 says:  == too low
> p2 knows the range is `> 6 && <= 10` so the next guess is a range of `4 / 2 + 6 = (2+6) = 8` 
> p1 says: too high
> p2 knows the range is `> 6 && < 8` so the answer is `7`

5. Fibonacci Series: 
Write a function that computes the list of the first 100 Fibonacci numbers. The first two Fibonacci numbers are 1 and 1.

Fibonacci series is calculated by adding n + the last number in the series. 
1+1 = 2, 2+1 = 3, 3+2 = 5, 5+3=8

The n+1-st Fibonacci number can be computed by adding the n-th and the n-1-th Fibonacci number. The first few are therefore 1, 1, 1+1=2, 1+2=3, 2+3=5, 3+5=8.

6. Write a function that takes a number and returns a list of its digits. So for 2342 it should return [2,3,4,2].  Caveat:  Do not use substring manipulation, use arithmetic and modulus.  The modulo operator returns the remainder after integer division. 

e.g.  `4 mod 2 = 0` and `5 mod 2 = 1`

## Arrays / Lists

1. Write a function that rotates a list by k elements. For example [1,2,3,4,5,6] rotated by two becomes [3,4,5,6,1,2]. Try solving this without creating a copy of the list. How many swap or move operations do you need?

2. Write a function that tests whether a string is a palindrome.

3. Write a function that takes a list of strings an prints them, one per line, in a rectangular frame. For example the list 

```
["Hello", "World", "in", "a", "frame"] 
```
gets printed as:

```
*********
* Hello *
* World *
* in    *
* a     *
* frame *
*********
```

4. Take a multiply subscripted list of numbers, calculate the average of each subscripted list, rounded to the nearest integer, then print each result as a histogram

e.g. 
The average is calculated as the sum of each number in list / length of list

A list like this:
```
[
  [4, 23, 54, 12, 45],
  [6, 23, 56, 32, 2, 4],
  [56, 34, 23, 1, 56, 6],
  [76, 54, 23, 65, 12]
]    
```

results in a histogram that looks like:
```
28 | ***************************
21 | ********************
30 | *****************************
46 | **********************************************
```



